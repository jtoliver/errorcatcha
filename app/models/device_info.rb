class DeviceInfo
  include Mongoid::Document

  embedded_in :error_message

  field :env, type: String
  field :device_id, type: String
  field :store_id, type: String
  field :content_set_version_id, type: String
  field :content_set_id, type: String
  field :session_id, type: String
  field :video_name, type: String
end
