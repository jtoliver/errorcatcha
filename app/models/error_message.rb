class ErrorMessage
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Search

  embeds_one :device_info

  field :error_message, type: String
  field :line_number, type: Integer

  search_in :line_number, device_info: [
    :device_id,
    :store_id,
    :content_set_version_id,
    :content_set_id,
    :session_id,
    :video_name
  ]
end
