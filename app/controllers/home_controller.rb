class HomeController < ApplicationController
  before_action :authenticate_user!

  respond_to :json, only: [:show, :publish]

  def index
    @error_messages = ErrorMessage.all if !params[:search].present?
    @error_messages = ErrorMessage.full_text_search(params[:search]).page(params[:page]).per(25) if params[:search].present?

    if params[:daterange].present?
      daterange = params[:daterange].split(' - ')
      @error_messages = @error_messages.where(:created_at.gte => parse_date(daterange[0]), :created_at.lte => parse_date(daterange[1]))
    end

    @error_messages = @error_messages.order_by(:created_at => 'desc').page(params[:page]).per(25)

    respond_to do |format|
      format.html
    end
  end

  def show
    @error_message = ErrorMessage.find(params[:id])
    render json: @error_message
  end

  def publish
    @error_message = ErrorMessage.create(message_params)
    @error_message.device_info = DeviceInfo.new(message_params[:device_info])

    respond_to do |format|
      format.json {
        if @error_message.save!
          render json: { status: 200, message: 'Message saved!' }
        else
          render json: { status: 0, message: 'Message not saved :(' }
        end
      }
    end
  end

  private

  def parse_date(date)
    DateTime.strptime date, "%m/%d/%Y %l:%M %p"
  end

  def message_params
    params.permit(
      :error_message,
      :line_number,
      device_info: [
        :env,
        :device_id,
        :store_id,
        :content_set_version_id,
        :content_set_id,
        :session_id,
        :video_name
      ]
    )
  end
end
