// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require moment
//= require handlebars.runtime
//= require_tree ./templates
//= require_tree .

$(document).on('page:load', initialize);
$(document).ready(initialize);

function initialize() {
  $('table tbody').on('click', 'tr', function(event) {
    var modal = $('#messageModal');
    var button = $(this);

    modal.on('show.bs.modal', function (e) {
      var id = button.data('message-id');
      var modal = $(this)
      if (id) {
        $.ajax({
          url: './message',
          type: 'GET',
          data: { id: id },
          dataType: 'json',
          success: function(data) {
            console.log(data)
            if (data) {
              modal.html(HandlebarsTemplates.message(data));
            }
          },
          error: function(data) {
            console.log(data)
          }
        });
      }
    });

    modal.modal('show')
  });

  $('input[name="daterange"]').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    locale: {
      format: 'MM/DD/YYYY h:mm A'
    }
  });

  setTimeout(function(){
    $('.alert').fadeOut(500);
  }, 3000);
}
