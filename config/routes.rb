Rails.application.routes.draw do
  devise_for :users
  root to: "home#index"

  post 'publish' => 'home#publish', defaults: { format: 'json' }
  get 'message' => 'home#show', defaults: { format: 'json' }
end
